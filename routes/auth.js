var models = require('../models');
var express = require('express');
var router = express.Router();

router.post('/', (req , res, next) => {
    //console.log(req.body);
    models.user.authenticate(req.body.username, req.body.password)
    .then((result) => {
        if (result.access) {
            req.session.userId = result.user.id;
            res.json(result);
        } else if (result.user) {
            res.json(401, result);
        } else {
            res.json(401,result);
        }
    });
});

module.exports = router;