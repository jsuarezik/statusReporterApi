var models = require('../models');
var express = require('express');
var status = require('../utils/status')
var router = express.Router();

router.get('/', (req , res) => {
    models.user.findAll({include : [{model : models.miner}]})
        .then((users) => {
            res.json(users);
        });
});

router.get('/:id', (req, res) => {
    models.user.findById(req.params.id, { include : {model : models.miner}})
        .then((user) => {
            res.json(user);
        })
});

router.get('/:id/miners/status', (req, res) => {
    let includes = [
        {
            model: models.user,
            where : {
                id : req.params.id
            },
            through : {
                attributes : []
            }
        },
        {
            model : models.datacenter
        }
    ];
    models.miner.findAll({ 
        include : includes
    })
    .then ( async miners => {
            let results = [];
            limit = req.query.limit;
            if (limit) miners = miners.slice(0,5);
            
            for (let i = 0; i < miners.length; i++) {
                await status.getStatus(miners[i]).then( response => results.push(response)).catch((err) => console.log(err));
            }

            res.json(results);
        });
});

module.exports = router;