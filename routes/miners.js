var models = require('../models');
var express = require('express');
var router = express.Router();

router.get('/', (req , res) => {
    models.miner.findAll({ include : [ {model : models.user}, {model: models.datacenter}]})
        .then((miners) => {
            res.json(miners);
        });
});

router.get('/:id', (req, res) => {
    models.miner.findById(req.params.id, { include : [ {model : models.user}]})
        .then((miner) => {
            res.json(miner);
        })
});

router.post('/:id/reboot', (req, res) => {
    models.miner.findById(req.params.id, { include : [ {model : models.datacenter}]})
        .then((miner) => {
            status.reboot(miner);
        });
    res.json(204, {});
});

module.exports = router;