var models = require('../models');
var express = require('express');
var router = express.Router();

router.get('/', (req , res) => {
    models.datacenter.findAll({ include : [{model : models.miner}]})
        .then((datacenters) => {
            res.json(datacenters);
        });
});

router.get('/:id', (req, res) => {
    models.datacenter.findById(req.params.id, { include : [{model : models.miner}]})
        .then((datacenter) => {
            res.json(datacenter);
        })
});

module.exports = router;