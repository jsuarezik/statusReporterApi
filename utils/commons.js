module.exports.createResultPool = function (string) {
    tokens = string.split('\n');
    validTokens = [];
    pools = [];
    for (let i = 0; i < tokens.length; i++) {
        if (tokens[i].indexOf('total') !== -1) break;
        if (tokens[i].indexOf('id="cbi-table-1-pool"') !== -1) validTokens.push(tokens[i]); //Get the pool number
        if (tokens[i].indexOf('id="cbi-table-1-url"') !== -1) validTokens.push(tokens[i]); //Get the pool url
        if (tokens[i].indexOf('id="cbi-table-1-user"') !== -1) validTokens.push(tokens[i]); //Get the pool user
        if (tokens[i].indexOf('id="cbi-table-1-status"') !== -1) validTokens.push(tokens[i]); //Get the pool status
    }
        
    for (let i = 0; i  < validTokens.length; i+=4) {
        pool = { 
            'pool' : getData(validTokens[i]),
            'url' : getData(validTokens[i+1]),
            'user' : getData(validTokens[i+2]),
            'status' : getData(validTokens[i+3])
        };
        pools.push(pool);
    }
    
    return pools;
}

module.exports.createResultTemp = function (string) {
    tokens = string.split('\n');
    validTokens = [];
    temps = [];
    for (let i = 0; i < tokens.length; i++) {
        if (tokens[i].indexOf('Total') !== -1) break;
        if (tokens[i].indexOf('id="cbi-table-1-chain"') !== -1) validTokens.push(tokens[i]); //Get the chain
        if (tokens[i].indexOf('id="cbi-table-1-temp2"') !== -1) validTokens.push(tokens[i]); //Get the chip temp
    }

    for (let i = 0; i < validTokens.length; i+=2) {
        temp = {
            'chain' : getData(validTokens[i]), 
            'temp' : getData(validTokens[i+1])
        };
        temps.push(temp);
    }

    return temps;
}

getData = function(string) {
    return string.substring(string.indexOf('>') + 1, string.indexOf('</'));
}