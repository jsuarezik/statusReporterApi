const utils = require('./commons');
const rp = require('request-promise');
const cheerio = require('cheerio');

const AuthHeader = 'Digest username="root", realm="antMiner Configuration", nonce="6d52f6f42f4853aa79764c50370531ad", uri="/cgi-bin/minerStatus.cgi", response="551d8ca6199945cc852b4869aeb06bad", qop=auth, nc=00000013, cnonce="ee7c433c63a237c5"';

getPools = function($) {
    rawhtml = $('#ant_pools').html();
    pools = utils.createResultPool(rawhtml);

    return pools;
}

getChipTemp = function($) {
    rawHtml = $('#ant_devs').html();
    temps = utils.createResultTemp(rawHtml);

    return temps;
}

getUrl = (miner, uri) => {
    domain = miner.datacenter.url;
    uri = uri;
    port = miner.port;
    
    return domain.concat(':'.concat(port).concat(uri));
}

module.exports.getStatus = async (miner) => {
    var options = {
        uri: getUrl(miner, '/cgi-bin/minerStatus.cgi'),
        method : 'GET',
        headers: {
            'Authorization': AuthHeader
        },
        transform: function (body) {
            return cheerio.load(body);
        }
    };
    value = await rp(options)
        .then(($) => {
            var response = {
                'id' : miner.id,
                'name' : miner.name,
                'elapsed' : $('#ant_elapsed').text() || 0,
                'hashrate' : $('#ant_ghs5s').text() || 0,
                'pools' : getPools($) || [],
                'temp' : getChipTemp($) || []
            };
            return Promise.resolve(response);
        })
        .catch((err) => {
            return Promise.reject([]);
        })

    return value;
};

module.exports.reboot = async (miner) => {
    var options = {
        uri : getUrl(miner, 'cgi-bin/reboot.cgi'),
        method : 'GET',
        headers : {
            'Authorization' : AuthHeader
        }
    };

    value = await rp(options)
    .then(() => {
        return Promise.resolve('success');
    })
    .catch((err) => {
        return Promise.reject(err);
    });

};

