const express = require('express');
const app = express();
const rp = require('request-promise');
const cheerio = require('cheerio');
const bodyParser = require('body-parser');
const session = require('express-session');

//My modules
const status = require('./utils/status')
const models = require('./models');
const routes = require('./routes/routes');
const users = require('./routes/users');
const miners = require('./routes/miners');
const datacenters = require('./routes/datacenters');
const auth = require('./routes/auth');

//Config
app.set('json_spaces', 4);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())
//use sessions for tracking logins
app.use(session({
    secret: 'remote asic',
    resave: true,
    saveUninitialized: false
}));

//Start
app.listen(8080, function () {
    console.log('App Listening on port 3001!');
});

app.use('/', routes);
app.use('/users', users);
app.use('/miners', miners);
app.use('/datacenters', datacenters);
app.use('/login', auth)

app.get('/status',  (req, res) => {
    models.miner.findAll({
        include : [{model : models.datacenter} ]
    })
    .then(async (miners) => {
        results = [];
        for (let i = 0; i < miners.length; i++) {
            await status.getStatus(miners[i]).then((response) => results.push(response)).catch((err) => console.log(err));
        }
                
        res.send(results);
    });
});

app.get('/reboot', (req, res) => {
    uri = 'http://192.168.1.66/cgi-bin/reboot.cgi';
    var options = {
        uri: uri,
        method : 'GET',
        headers: {
            'Authorization': AuthHeader
        }
    };
    rp(options)
        .then(() => {
            console.log('success')
        })
        .catch((err) => {
            console.log(err)
        });

    res.send({message: "success"});

});
