'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('miners', [{
        name: 'local test',
        port: '80',
        datacenter_id : 1
      }, {
        name: 'remote test',
        port: '80',
        datacenter_id : 2
      },
      {
        name: 'remote test',
        port: '80',
        datacenter_id : 2
      },
      {
        name: 'remote test',
        port: '80',
        datacenter_id : 2
      }], {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('miners', null, {});
  }
};
