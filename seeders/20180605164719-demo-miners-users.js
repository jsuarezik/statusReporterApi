'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('miners_users', [{
        user_id : 1,
        miner_id : 1
      },
      {
        user_id : 1,
        miner_id : 2
      },
      {
        user_id : 1,
        miner_id : 3
      },
      {
        user_id : 1,
        miner_id : 1
      }], {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('miners_users', null, {});
  }
};
