'use strict';
const bcrypt = require ('bcrypt');

module.exports = {
  up:(queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('users', [{
        username: 'root',
        password: bcrypt.hashSync('root', 10),
        email: 'root@root.com'
      }], {});
  },

  down: (queryInterface, Sequelize) => {
    down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('users', null, {});
    }
  }
};
