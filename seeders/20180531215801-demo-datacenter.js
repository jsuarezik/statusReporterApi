'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('datacenters', [{
        name: 'Local',
        url : 'http://192.168.1.66'
      }, {
        name : 'Remote',
        url : 'http://173.24.234.42'
      }], {});
  },
  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('datacenters', null, {});
  }
};
