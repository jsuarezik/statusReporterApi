'use strict';
const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('user', {
    username: { type : DataTypes.STRING, unique : true },
    password: DataTypes.STRING,
    email: DataTypes.STRING
  }, {underscored : true});
  
  User.associate = function(models) {
    User.belongsToMany(models.miner, { through : 'miners_users', foreignKey : 'user_id'});
  };

  User.authenticate = (username, password) => {
    return User.findOne({
      where : {
        username : username
      }
    })
    .then( user => {
      if (user) {
        var access = bcrypt.compareSync(password, user.password);
        var user = access ? user : null;
        return { 'access' : access, 'user': user};
      } else {
        return { 'access' : false, 'user': []}
      }
    });
  };
  
  return User;

};