'use strict';
module.exports = (sequelize, DataTypes) => {
  var Miner = sequelize.define('miner', {
    name: DataTypes.STRING,
    port: DataTypes.INTEGER
  }, { underscored : true});

  Miner.associate = function(models) {
    Miner.belongsTo(models.datacenter, {foreignKey : 'datacenter_id'});
    Miner.belongsToMany(models.user, {through : 'miners_users', foreignKey: 'miner_id'})
  };
  
  return Miner;
};