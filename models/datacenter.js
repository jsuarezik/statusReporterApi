'use strict';
module.exports = (sequelize, DataTypes) => {
  var Datacenter = sequelize.define('datacenter', {
    name: DataTypes.STRING,
    url: DataTypes.STRING
  }, {underscored : true});

  Datacenter.associate = function(models) {
    Datacenter.hasMany(models.miner);
  };
  return Datacenter;
};