'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('miners', 'datacenter_id', {
      type : Sequelize.INTEGER,
      allowNull: false,
      references : {
        model : 'datacenters',
        key : 'id'
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('miners', 'datacenter_id');
  }
};
