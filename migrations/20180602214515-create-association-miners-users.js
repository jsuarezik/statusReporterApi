'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('miners_users', {
        user_id : {
          type : Sequelize.INTEGER,
          primaryKey : true,
          references : {
            model : 'users',
            key : 'id'
          }
        },
        miner_id : {
          type : Sequelize.INTEGER,
          primaryKey : true,
          references : {
            model : 'miners',
            key : 'id'
          }
        },
        created_at: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue : Sequelize.literal('CURRENT_TIMESTAMP')
        },
        updated_at: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue : Sequelize.literal('CURRENT_TIMESTAMP')
        }
      });
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.dropTable('miners_users');
  }
};
